import XCTest
@testable import SPM_RemoteImageView

final class SPM_RemoteImageViewTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(SPM_RemoteImageView().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}

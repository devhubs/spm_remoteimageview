import XCTest

import SPM_RemoteImageViewTests

var tests = [XCTestCaseEntry]()
tests += SPM_RemoteImageViewTests.allTests()
XCTMain(tests)
